﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UPB.Practice3.Data.Models;

namespace UPB.Practice3.Data
{
    public class DbContext : IDbContext
    {

        //Access to db server
        //Reference to db tables (entities)

        public List<Group> GroupTable { get; set; }

        public int cont { get; set; }

        public DbContext() 
        {
            //Read from config file the DB connection string
            //ORM for net core is (Entity Framework Core)
            //Review Repository Pattern
            GroupTable = new List<Group>();
            string[] nombres = { "Nirvana", "GunsNRoses", "OneRepublic", "Queen", "One Direction", "5SOS", "The Cramberries", "The Doors", "The Fray", "Snow Patrol" };
            string[] apellidos = { "Allende", "Gonzales", "Estenssoro", "Jobs", "Sarmiento", "Duran", "Rosales", "Zambrana", "Andrade", "Burgos" };

            Random r = new Random();
            int est = r.Next(5, 11);

            cont = 0;

            for (int i = 0; i < est; i++)
            {
                cont += 1;
                string med = cont >= 100 ? "" + cont : (cont >= 10 ? "0" + cont : "00" + cont);
                GroupTable.Add(new Group()
                {
                    Name = nombres[r.Next(0, 10)],
                    AvailableSlots = r.Next(1, 51),
                    Id = uniqueId(cont)
                }) ;
            }
        }

        public string uniqueId(int c) 
        {
            string med = c >= 100 ? "" + c : (c >= 10 ? "0" + c : "00" + c);
            return $"Group-" + med;
        }

        public int toMyInt(string s)
        {
            string result = System.Text.RegularExpressions.Regex.Match(s, @"\d+").Value;
            int a = Int32.Parse(result);
            return a;
        }

        public Group AddGroup(Group group) 
        {

            if (String.IsNullOrEmpty(group.Id) || group.AvailableSlots <= 0) 
            {
                Console.Out.WriteLine("ID NULL o AvailableSlots INVALIDO: el ID o AvailableSlots del grupo esta vacio o Null");
                throw new Exception("ID NULL o AvailableSlots INVALIDO: el ID o AvailableSlots del grupo esta vacio o Null");
            }

            if (group.Name.Length> 50|| String.IsNullOrEmpty(group.Name))
            {
                Console.Out.WriteLine("El nombre tiene un largo > 50 o el nombre del grupo esta vacio");
                throw new Exception("El nombre tiene un largo > 50 o el nombre del grupo esta vacio");
            }
            int idgrupo = toMyInt(group.Id);
            List<Group> matches = GroupTable.FindAll(gr => gr.Id == group.Id);

            if (idgrupo <= cont || matches.Count>0) 
            {
                cont = toMyInt(GroupTable.Last().Id);
                cont += 1;
                
                group.Id = uniqueId(cont);
                Console.Out.WriteLine("Se actualizo el ID a uno valido: " +group.Id);
            }
            
            GroupTable.Add(group);
            return group;

        }
        public List<Group> GetAllGroups()
        {
           
            return GroupTable;

        }
        public Group UpdateGroup(Group groupToUpdate)
        {
            if (String.IsNullOrEmpty(groupToUpdate.Id) ||groupToUpdate.Name.Length > 50 || String.IsNullOrEmpty(groupToUpdate.Name))
            {
                Console.Out.WriteLine("El nombre para actualizar tiene un largo > 50 o esta vacio o ID null");
                throw new Exception("El nombre para actualizar tiene un largo > 50 o esta vacio o ID null");
            }

            Group foundGroup = GroupTable.Find(group => group.Id == groupToUpdate.Id);
            foundGroup.Name = groupToUpdate.Name;
            return foundGroup;

        }
        public Group DeleteGroup(Group groupToDelete)
        {
            if (String.IsNullOrEmpty(groupToDelete.Id) )
            {
                Console.Out.WriteLine("ID NULL: el ID del grupo esta vacio o Null");
                throw new Exception("ID NULL: el ID del grupo esta vacio o Null");
            }
            GroupTable.RemoveAll(group => group.Id == groupToDelete.Id);


            
            return groupToDelete;

        }



    }
}
