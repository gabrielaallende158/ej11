﻿using System;
using System.Collections.Generic;
using System.Text;
using UPB.Practice3.Data.Models;

namespace UPB.Practice3.Data
{
    public interface IDbContext
    {



        public Group AddGroup(Group student);

        public List<Group> GetAllGroups();

        public Group UpdateGroup(Group studentToUpdate);

        public Group DeleteGroup(Group studentToDelete);
        
    }
}
