﻿using System;

namespace UPB.Practice3.Data.Models
{
    public class Group
    {

        //DAO = Data Access Object

        
        public string Name { get; set; }
        public int AvailableSlots { get; set; }
        public string Id { get; set; }
    }
}
