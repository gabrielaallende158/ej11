﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using UPB.Practice3.Logic.Models;
using UPB.Practice3.Logic.Managers;

namespace ej03.Controllers
{
    [ApiController]
    [Route("/api/students")]


    public class GroupsController : ControllerBase
    {

        private static IConfiguration _config;
        private readonly IGroupManager _groupManager;
        


        string[] nombres = { "Camila", "Fernando", "Fabricio", "Jose", "Andrea", "Stephany", "Carlos", "Nicole", "Kiara", "Matilde" };
        string[] apellidos = { "Allende", "Gonzales", "Estenssoro", "Jobs", "Sarmiento", "Duran", "Rosales", "Zambrana", "Andrade", "Burgos" };

        public GroupsController(IConfiguration config, IGroupManager groupManager)
        {
            _config = config;
            _groupManager = groupManager;
        }

        [HttpGet]
        public List<Group> GetGroup()
        {
            return _groupManager.GetAllGroups();
        }

        [HttpPost]
        public Group CreateGroup([FromBody] Group group)//, [FromBody] string studentLastName ) 
        {
            return _groupManager.CreateGroup(group);
        }
        [HttpPut]
        public Group UpdateGroup([FromBody] Group group)
        {
            return _groupManager.UpdateGroup(group);
        }

        [HttpDelete]
        public Group DeleteGroup([FromBody] Group group)
        {
            return _groupManager.DeleteGroup(group);
        }
    }
}
