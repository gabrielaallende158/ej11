﻿using System;
using System.Collections.Generic;
using System.Text;
using UPB.Practice3.Logic.Models;
//using UPB.Practice3.Data.Models;

namespace UPB.Practice3.Logic.Models
{
    public static class DTOMappers
    {
        public static List<Group> MapGroups (List<Practice3.Data.Models.Group> groups) 
        {
            List<Group> mappedGroups = new List<Group>();
            foreach (Practice3.Data.Models.Group group in groups) 
            {
                mappedGroups.Add(new Group() 
                {
                    Id = group.Id,
                    Name = group.Name,
                    AvailableSlots = group.AvailableSlots
                });
            }

            return mappedGroups;
        }

        public static Group MapGroupDL(Practice3.Data.Models.Group group) 
        {
            Group myLogicGr = new Group()
            {
                Id = group.Id,
                Name = group.Name,
                AvailableSlots = group.AvailableSlots
            };

            return myLogicGr;
        }

        public static Practice3.Data.Models.Group MapGroupLD(Group group)
        {
            Practice3.Data.Models.Group myDataGr = new Practice3.Data.Models.Group()
            {
                Id = group.Id,
                Name = group.Name,
                AvailableSlots = group.AvailableSlots
            };

            return myDataGr;
        }
    }
}
