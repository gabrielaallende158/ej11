using System;

namespace UPB.Practice3.Logic.Models
{
    public class Group
    {
        //DTO = Data Transfer Object
        public string Name { get; set; }
        public int AvailableSlots { get; set; }
        public string Id { get; set; }
    }
}
