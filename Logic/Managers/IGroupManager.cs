﻿using System;
using System.Collections.Generic;
using System.Text;
using UPB.Practice3.Data;
using UPB.Practice3.Logic.Models;

namespace UPB.Practice3.Logic.Managers

{
    public interface IGroupManager
    {



        //[HttpGet]
        public List<Group> GetAllGroups();



        public Group CreateGroup(Group group);//, [FromBody] string studentLastName ) 



        public Group UpdateGroup(Group group);




        public Group DeleteGroup(Group group);
        
    }
}
