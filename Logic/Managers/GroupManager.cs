﻿using System;
using System.Collections.Generic;
using System.Text;

using UPB.Practice3.Data;
using UPB.Practice3.Logic.Models;



namespace UPB.Practice3.Logic.Managers
{
   
    
    public class GroupManager: IGroupManager
    {
        string[] nombres = { "Camila", "Fernando", "Fabricio", "Jose", "Andrea", "Stephany", "Carlos", "Nicole", "Kiara", "Matilde" };
        string[] apellidos = { "Allende", "Gonzales", "Estenssoro", "Jobs", "Sarmiento", "Duran", "Rosales", "Zambrana", "Andrade", "Burgos" };
        private IDbContext _dbContext;

        public GroupManager(IDbContext dbContext) 
        {
            _dbContext = dbContext;
        }

        //[HttpGet]
        public List<Group> GetAllGroups()
        {
            List<Data.Models.Group> grupo = _dbContext.GetAllGroups();
            
            return DTOMappers.MapGroups(grupo);

            //TERMINAR PARA LA PRACTICA
        }
        
        public Group CreateGroup(Group grupo)//, [FromBody] string studentLastName ) 
        {
            //return new Student()
            //{
            //    Name = studentName,
            //    LastName = "Allende"
            //};

            if (String.IsNullOrEmpty(grupo.Name))
            {
                throw new Exception();
            }

            _dbContext.AddGroup(DTOMappers.MapGroupLD(grupo));

            return grupo;
        }

        public Group UpdateGroup(Group grupo)
        {
            //estudiante.Name = "NuevoNombre";
            //estudiante.LastName = "NuevoApellido";

            Practice3.Data.Models.Group gr = _dbContext.UpdateGroup(DTOMappers.MapGroupLD(grupo));
            return DTOMappers.MapGroupDL(gr);
        }

       
        public Group DeleteGroup(Group grupo)
        {
            //estudiante.Name = "Deleted";
            //estudiante.LastName = "Deleted";

            Practice3.Data.Models.Group gr = _dbContext.DeleteGroup(DTOMappers.MapGroupLD(grupo));
            return DTOMappers.MapGroupDL(gr);
        }

        
    }
}
